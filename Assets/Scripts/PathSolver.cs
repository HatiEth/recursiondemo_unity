using UnityEngine;
using System.Collections;
using System;

public class PathSolver : MonoBehaviour {

	public Waypoint StartingPoint;
	public Waypoint TargetWaypoint;

	public SpriteRenderer spriteComp;

	void Update()
	{
		// Handle Input
		if(Input.GetKeyUp(KeyCode.Alpha1)) // On Key Up '1'
		{
			StartingPoint.testPathTo(TargetWaypoint);
		}
		if(Input.GetKeyUp(KeyCode.Alpha2)) // Bei Taste '2'
		{
			StartCoroutine(StartingPoint.testPathToAsync(TargetWaypoint));
		}
	}
}
