using UnityEngine;
using System.Collections;
using System;

public class BoolRef
{
	public bool value;
}

public class Waypoint : MonoBehaviour {

	public Waypoint[] children;

	
	public bool testPathTo(Waypoint target)
	{
		if(this == target) // Wenn 'ich' das 'target' bin habe ich einen Weg gefunden (Abbruchbedingung)
		{
			StartCoroutine(Colorize(Color.green));
			return true;
		}
		else // ansonsten gehe meine Kinder durch und teste diese
		{
			bool isPath = false;
			for(int i=0;i<children.Length;++i)
			{
				isPath = isPath || children[i].testPathTo(target);
				if (isPath)
					break;
			}
			if (isPath) StartCoroutine(Colorize(Color.green));
			else StartCoroutine(Colorize(Color.yellow));
			return (isPath);
		}
	}

	public IEnumerator testPathToAsync(Waypoint target)
	{
		GetComponent<SpriteRenderer>().color = Color.yellow; // Markiere aktuellen Knoten
		yield return new WaitForSeconds(1.0f); // Warte 1 Sec
		if(this == target) // Wenn am Ziel markiere grün und setze Wert
		{
			GetComponent<SpriteRenderer>().color = Color.green;
		}
		else
		{
			GetComponent<SpriteRenderer>().color = Color.red;
			foreach (var c in children)
			{
				yield return StartCoroutine(c.testPathToAsync(target)); 
				if(c.GetComponent<SpriteRenderer>().color == Color.green)
				{
					GetComponent<SpriteRenderer>().color = Color.green;
				}
			}
		}
	}


	/**
	 * Färbt einen Knoten für 3 Sekunden in der Farbe [c] ein.
	 * */
	IEnumerator Colorize(Color c)
	{
		GetComponent<SpriteRenderer>().color = c;
		yield return new WaitForSeconds(3.0f);
		GetComponent<SpriteRenderer>().color = Color.white;
	}


	/**
	 * Visual Stuff / Demo Stuff
	 */
	void OnDrawGizmos()
	{
		Gizmos.color = Color.yellow;
		foreach(var wp in children)
		{
			if(wp)
			{
				Gizmos.DrawLine(this.transform.position, wp.transform.position);
				Vector3 midPoint = Vector3.Lerp(transform.position, wp.transform.position, 0.75f);
				Vector3 dir = (wp.transform.position - transform.position).normalized;

				Quaternion q = Quaternion.FromToRotation(Vector3.right, dir);

				Gizmos.DrawLine(midPoint, midPoint + q * new Vector3(-0.1f, -0.1f));
				Gizmos.DrawLine(midPoint, midPoint + q * new Vector3(-0.1f, 0.1f));
			}
		}
	}

	public void GLDrawLines()
	{
		foreach (var wp in children)
		{
			GL.Begin(GL.LINES);
			{
				GL.Color(Color.yellow);
				GL.Vertex3(transform.position.x, transform.position.y, transform.position.z);
				GL.Vertex3(wp.transform.position.x, wp.transform.position.y, wp.transform.position.z);
				Vector3 midPoint = Vector3.Lerp(transform.position, wp.transform.position, 0.75f);
				Vector3 dir = (wp.transform.position - transform.position).normalized;

				Quaternion q = Quaternion.FromToRotation(Vector3.right, dir);

				var p = midPoint + q * new Vector3(-0.1f, -0.1f);
				GL.Vertex3(midPoint.x, midPoint.y, midPoint.z);
				GL.Vertex3(p.x, p.y, p.z);

				p = midPoint + q * new Vector3(-0.1f, 0.1f);
				GL.Vertex3(midPoint.x, midPoint.y, midPoint.z);
				GL.Vertex3(p.x, p.y, p.z);
			}
			GL.End();

			wp.GLDrawLines();
		}
	}
}
