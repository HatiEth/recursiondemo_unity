using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CameraPPLines : MonoBehaviour {

	Material mat;

	void OnPostRender()
	{
		if (!mat)
		{
			// Unity has a built-in shader that is useful for drawing
			// simple colored things. In this case, we just want to use
			// a blend mode that inverts destination colors.			
			var shader = Shader.Find("Hidden/Internal-Colored");
			mat = new Material(shader);
			mat.hideFlags = HideFlags.HideAndDontSave;
			// Set blend mode to invert destination colors.
			mat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusDstColor);
			mat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
			// Turn off backface culling, depth writes, depth test.
			mat.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
			mat.SetInt("_ZWrite", 0);
			mat.SetInt("_ZTest", (int)UnityEngine.Rendering.CompareFunction.Always);
		}
		mat.SetPass(0);
		FindObjectOfType<PathSolver>().StartingPoint.GLDrawLines();
	}
}
